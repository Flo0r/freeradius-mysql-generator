﻿using System.Data;

namespace Freeradius.MySQLToUsersFile
{
	class MainClass
	{
		public static void Main (string[] args)
		{
            /*
             * Utilisation de la dll pour MySQL
             **  cd /usr/lib/mono/2.0/
             **  cp /app/Freeradius.MySQLToUsersFile/Freeradius.MySQLToUsersFile/MySQL/2/mysql.* .
             **  mv  mysql.data.dll MySql.Data.dll
             **  gacutil -i MySql.Data.dll
             * 
             * Build du projet
             **  xbuild Freeradius.MySQLToUsersFile.sln
             **  mono Freeradius.MySQLToUsersFile.exe
             */

            // Récupération des données de la base MySQL
            MYSQL myConnexion = new MYSQL("Server=localhost;Database=radiusManager;User ID=DataIntegrator;Password=DataIntegratorPwd;");
			myConnexion.setRequest ("SELECT macAddress, vlan FROM `wifi_radius`;");
			IDataReader result = null;
            myConnexion.ExecuteRequest(ref result);

            // Transformation des données en fichier plat
            DataIntegrator integrate = new DataIntegrator(ref result, "/etc/freeradius/users", ':');
            integrate.Transform();
            integrate.Close();
		}
	}
}
