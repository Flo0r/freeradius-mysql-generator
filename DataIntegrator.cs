using System;
using System.IO;
using System.Data;

namespace Freeradius.MySQLToUsersFile
{
	public class DataIntegrator
    {
        String sPath = "";
        Char cMacSeparator;
        Char cMacSeparatorToReplace;
        IDataReader v_Reader = null;

        /**
            Constructeur du DataIntegrator
            p_Reader = référence vers l'itérateur des données
            p_Path = Path vers le fichier à enregistrer
            p_MacSeparator & p_MacSeparatorToReplace = Séparateurs à remplacer selon le format attendu de l'adresse MAC
        */  
        public DataIntegrator(ref IDataReader p_Reader, String p_Path, Char p_MacSeparator, Char p_MacSeparatorToReplace = ':') {
            sPath = p_Path;
            cMacSeparator = p_MacSeparator; 
            cMacSeparatorToReplace = p_MacSeparatorToReplace; 
            v_Reader = p_Reader;
        }

        /**
        Permet de transformer des données provenant d'un itérateur et de les enregistrer dans un fichier selon le format Freeradius
         */
        public void Transform() {
            if(null == v_Reader) {
                return;
            }

			string generatedString = "";

			File.Delete (sPath);

			while (v_Reader.Read()) {

                if("" != generatedString) generatedString += "\n\n";

                generatedString +=  v_Reader[0].ToString().Replace(cMacSeparator, cMacSeparatorToReplace) + " Auth-Type := EAP\n" +
                       "        Tunnel-type = VLAN,\n" +
                       "        Tunnel-Medium-Type = 802,\n" +
                       "        Tunnel-Private-Group-ID = " + v_Reader[1].ToString();
			}

            // Inscription des données dans le fichier plat
            if (!File.Exists (sPath)) {
                File.WriteAllText (@sPath, generatedString);
            } else {
                File.AppendAllText (@sPath, generatedString);
            }
            // Une fois que l'on a autorisé les adresses MACs, les autres doivent être rejetée.
			if (File.Exists (sPath)) {
				File.AppendAllText (@sPath, "\n\nDEFAULT Auth-Type := Reject\n" 
					+ "        Reply-Message = \"Access rejected.\"");
			}

            Console.WriteLine ("Success file generation !");
        }

        /**
        Fermeture de l'itérateur
        */
        public void Close () {
            if(null != v_Reader) {
                v_Reader.Close();
            }
        }
	}
}
