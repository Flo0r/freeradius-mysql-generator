using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace Freeradius.MySQLToUsersFile
{
	public class MYSQL
	{
		private string s_ConnexionString;
		private IDbConnection v_DbConnexion;
		private IDbCommand v_DbCommand;
		private string sqlRequest;

		private bool canStart = false;

		/** 
		Ouvre une connexion vers la base de données
		*/
		public MYSQL (string aConnexionString)
		{
			try {
				this.s_ConnexionString = aConnexionString;

				this.v_DbConnexion = new MySqlConnection(aConnexionString);
				this.v_DbConnexion.Open();

				canStart = true;
			} catch (MySqlException) {
				System.Console.WriteLine ("Can't connect to DB !");
			}
		}

		/** 
		Execute une requête et récupère les données
		 */
		public void ExecuteRequest(ref IDataReader p_Reader) 
		{
			if(!canStart)
				return;

			try {
				this.v_DbCommand = this.v_DbConnexion.CreateCommand ();
				this.v_DbCommand.CommandText = this.sqlRequest;
				p_Reader = this.v_DbCommand.ExecuteReader ();

			} catch (MySqlException p_Error) {
				Console.WriteLine (p_Error.ToString());

				return;
			}
		}
		/**
			Permet d'initialiser la requête SQL
		 */
		public void setRequest(string anSqlRequest) {
			this.sqlRequest = anSqlRequest;
		}

		~MYSQL() {
			this.v_DbCommand.Dispose();
			this.v_DbCommand = null;

			this.v_DbConnexion.Close();
			this.v_DbConnexion = null;
		}
	}
}